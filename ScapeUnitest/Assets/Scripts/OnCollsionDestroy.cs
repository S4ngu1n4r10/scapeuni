﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollsionDestroy : MonoBehaviour
{
    public string ObjectTag;
    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(ObjectTag))
        {
            Destroy(gameObject);
        }
    }

}
