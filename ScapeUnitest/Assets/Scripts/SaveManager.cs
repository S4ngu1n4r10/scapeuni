﻿
using System.IO;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveManager
{
    public static void SavePlayer(PlayerController player)
    {
        
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Path.Combine(Application.persistentDataPath + "/scapeuni");
        FileStream stream = new FileStream(path, FileMode.Create);
        CharacterData data = new CharacterData(player);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static CharacterData LoadPlayer()
    {
        string path= Path.Combine(Application.persistentDataPath + "/scapeuni");
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            CharacterData data = formatter.Deserialize(stream) as CharacterData;
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("save file not found in " + path);
            return null ;
        }
    }
}

