﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRooms : MonoBehaviour
{
    // Start is called before the first frame update

    public LayerMask whatIsRoom;
    public LevelGeneration levelGen;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Collider2D roomDetection = Physics2D.OverlapCircle(transform.position, 5, whatIsRoom);

        if (roomDetection == null && levelGen.stopGeneration == true)
            //Spawn Random Room
        {
            int rand = Random.Range(0, levelGen.rooms.Length);
         GameObject instance= Instantiate(levelGen.rooms[rand], transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        else if (roomDetection!=null)

         {
            Destroy(gameObject);
            return;
        }
    }
}
