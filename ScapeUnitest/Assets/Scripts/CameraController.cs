﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    
    // vars
    public Transform target;

    private Vector3 offset;

    // init
    private void Start()
    {
        target =FindObjectOfType<PlayerController>().transform;
        offset = transform.position - target.position;	
    }

    // loop
    private void Update() {
		
        transform.position = target.position + offset;
        transform.LookAt(target);
    }
}