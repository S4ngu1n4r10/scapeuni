﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    Light candle;
    // Start is called before the first frame update
    void Start()
    {
        candle = GetComponent<Light>();
        StartCoroutine(Flash());
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Flash()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(.1f,.3f));
            flasheo();
            
        }
    }

    private void flasheo()
    {
        int randIntensity = Random.Range(0,2);
        if (randIntensity==0)
        {
            candle.intensity =8f;
        }
        else if (randIntensity==1)
        {
            candle.intensity = 9f;
        }
    }
}
