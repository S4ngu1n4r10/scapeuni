﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SpawnEnemys : MonoBehaviour {

    public GameObject[] objects;
    private Transform parent;
    public int maxEnemys=10;
    public bool Destroyed;
    public float TimeRemaining=500;
    public GameObject player;
    public float range=10;
    private bool spawneo;
    public Image barra;
    [SerializeField] private float timeBetweenSpawns = 2, health, currentHealth;
    private int EnemysSpawned;
   

    // Start is called before the first frame update
    void Start()
    { //en el inicio instancio los enemigos maximos;

        player = GameObject.FindGameObjectWithTag("Player");
        spawneo = true;
        EnemysSpawned = 0;
        Destroyed = false;
        currentHealth = health;
        parent = GetComponentInParent<Transform>();
      
       


    }

    // Update is called once per frame
    void LateUpdate()
    {
       // Checkhealth();
        timeBetweenSpawns +=Time.deltaTime;
        if (Vector3.Distance(transform.position, player.transform.position) < range && spawneo == true)
        {
                        IniciaSpawneo();
        }

        if (Destroyed)
        {
            TimeRemaining -= Time.deltaTime;
            spawneo = false;
            if (TimeRemaining <= 0f)
            {
                currentHealth = health;
                Debug.Log("tiempo a cero");

                Destroyed = false;
                EnemysSpawned = 0;
                spawneo = true;
                barra.fillAmount = currentHealth / health;
                IniciaSpawneo();

            }
        }
        if (!Destroyed)
        {
            TimeRemaining = 5;
        }
    }

    private void InstanciaEnemys()
    { int rand = Random.Range(0, objects.Length);
        Instantiate(objects[rand], transform.position,Quaternion.identity);
        EnemysSpawned++;
        timeBetweenSpawns = 0;
    }

    private void IniciaSpawneo()
    {
        if (spawneo)
        {
            if (timeBetweenSpawns>=2&& EnemysSpawned<maxEnemys)
            {
                InstanciaEnemys();
            }
            else { return; }
        }
    }

    public void takedamage(float amount)
    {
        if (!Destroyed)
        {
            if (barra != null)
            {
                barra.transform.DOPunchScale(new Vector2(.1f, .1f), .4f, 1, 1); 
                // Reduce the current health by the damage amount.
                currentHealth -= amount;
            // Set the health bar's value to the current health.
                barra.fillAmount = currentHealth / health;
            }
        

            if (currentHealth <= 0 && Destroyed == false)
            {
                currentHealth = 0;
                spawneo = false;
                barra.fillAmount = currentHealth / health;
                Destroyed = true;
            }
        }
        else
        {
            return;
        }
        }
    }
    /*private void Checkhealth()
    {
        if (currentHealth<=0)
        {
           currentHealth = 0;
            Destroyed = true;

        }
    }*/

