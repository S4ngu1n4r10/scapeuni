﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{
    public bool Red;
    public bool Gold;
    public bool Blue;
    public bool Green;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (Red)
            {
                PlayerController.NumKeysR++;
                Destroy(gameObject);

            }
            if (Blue)
            {
                PlayerController.NumKeysB++;
                Destroy(gameObject);

            }

            if (Green)
            {
                PlayerController.NumKeysGr++;
                Destroy(gameObject);

            }

            if (Gold)
            {
                PlayerController.NumKeysG++;
                Destroy(gameObject);

            }

        }
    }
}
