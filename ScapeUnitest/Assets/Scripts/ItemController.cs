﻿
using UnityEngine;


public class ItemController : MonoBehaviour
{
    [SerializeField] private bool hP, Res, Damage,Coins;
    private AudioManager audios;
    public Item item;

    public Sprite image => throw new System.NotImplementedException();

    public void OnPickup()
    {
        Debug.Log("pickinup " + item.name);
        Inventory.instance.Add(item);
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    private void Awake()
    {
        audios = FindObjectOfType<AudioManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("item");
        if (collision.CompareTag("Player"))
        {
            audios.Play("Pick");

            if (hP)
            {
              bool wasPickedUp=  Inventory.instance.Add(item);
                if (wasPickedUp)
                {
                    Destroy(gameObject);
                }
                Debug.Log("tecuro");
                collision.GetComponent<PlayerController>().currentHealth += 20;
                collision.GetComponent<PlayerController>().BarraSalud.fillAmount = collision.GetComponent<PlayerController>().currentHealth/  100;
            }
            if (Res)
            {
                Debug.Log("te doy recurso");
                collision.GetComponent<PlayerController>().currentResource += 20;
                collision.GetComponent<PlayerController>().BarraRecurso.fillAmount = collision.GetComponent<PlayerController>().currentResource / 100;
            }
            if (Coins)
            {
                PlayerController.NumCoins+=10;
            }

           // Destroy(gameObject);
        }


    }
}
