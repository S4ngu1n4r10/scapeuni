﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class ResourceController : MonoBehaviour
{

    public GameObject BarraSalud;
    // Start is called before the first frame update
    private void Start()
    {
        transform.localScale = new Vector3(1f, 1f);

    }

    public void SetSizeR(float sizeNormalized)
    {
        
        Vector3 scala = new Vector3(sizeNormalized, 1f);
        this.transform.localScale = Vector3.Lerp(transform.localScale, scala, 112);
       
    }

    // Update is called once per frame
    void Update()
    {


    }

   
}

