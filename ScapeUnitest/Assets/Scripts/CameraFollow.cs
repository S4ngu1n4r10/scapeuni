﻿
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject target;
    public float smoothSpeed = .125f;

   
   
    private void LateUpdate()
    {
          target = GameObject.FindGameObjectWithTag("Player");
        transform.position = target.transform.position;
    }
    
}
