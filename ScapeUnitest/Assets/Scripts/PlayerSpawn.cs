﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour

     
{public Transform[] playerStartingPos;
    private Vector3 playerSpawn;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        int randPlayerStartingPos = Random.Range(0, playerStartingPos.Length);
        playerSpawn = playerStartingPos[randPlayerStartingPos].position;
       Instantiate(Player, playerSpawn, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
