﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraSaludController : MonoBehaviour
{
    private Animator anim;
    public static bool hit;

   
    // Start is called before the first frame updat
    private void Start()
    {
        hit = false;
        anim = GetComponentInParent<Animator>();
      transform.localScale = new Vector3(1f, 1f);
       
    }

    public void SetSize(float sizeNormalized)
    {
       // anim?.SetBool("Hit", true);
        Vector3 scala = new Vector3(sizeNormalized, 1f);
        this.transform.localScale = Vector3.Lerp(transform.localScale, scala,112);
    }

    // Update is called once per frame
    void Update()
    {
       

    }

    
}
