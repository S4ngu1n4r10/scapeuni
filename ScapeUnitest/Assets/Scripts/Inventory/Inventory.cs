﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    // Start is called before the first frame update
    #region singleton
    public static Inventory instance;
  private void Awake()
    {
        if (instance!=null)
        {
            Debug.LogWarning("mas deuna instancia deinventario");
        }
        instance = this;
    }
    #endregion

    public List<Item> items = new List<Item>();
    public int spaceInv = 20;
    public int objectCount;
    
    public delegate void OnItemChanged();
    public OnItemChanged OnItemChangedCallback;

  
    public bool Add(Item item)
    {
        if (!item.isDefaultItem)
        {
            if (items.Count>=spaceInv)
            {
                
                Debug.Log("not room");
                return false;
            }
            if (items.Contains(item)&&item.cant<item.maxcant&&item.stackable)
            {
                item.cant++;
              
                Debug.Log("ya existe objeto en inventario");
                if (item.cant==5)
                {
                    item.numItem = item.cant;
                }
              
            }else
            {
                item.cant = 1;
                items.Add(item);
                objectCount++;
             

            }

            if (OnItemChangedCallback!=null)
            {   OnItemChangedCallback.Invoke();

            }
          
        }
        return true;
    }

    public void Remove(Item item)
    {
        items.Remove(item);
        objectCount--;
        if (OnItemChangedCallback != null)
        {
            OnItemChangedCallback.Invoke();

        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
