﻿
using UnityEngine;

public class ItemPickup : Interactable

    
{
    public Item item;
    public override void Interact()
    {
        base.Interact();
        Pickup();
    }
    
    void Pickup()
    {
        Debug.Log("pickinup "+ item.name);
        
        bool WasPickedUp=Inventory.instance.Add(item);
        if (WasPickedUp == false)
        {
            Debug.Log("no espacio de inventario");

        }
        if (WasPickedUp)
        {
           // Destroy(gameObject);

        }
        
    }
}
