﻿
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    // Start is called before the first frame update
 
    public GameObject inventoryUI;

    public Transform itemsParent;
   [SerializeField] private InventorySlot[] slots;
    void Start()
    {
         Inventory.instance.OnItemChangedCallback += UpdateUI;
        slots = itemsParent.GetComponentsInChildren<InventorySlot>();
    }

    // Update is called once per frame
    void Update()
    {
       
        
    }

    void UpdateUI()
    {

        for (int i = 0; i < slots.Length; i++)
        {
            if (i < Inventory.instance.items.Count)
            {
                slots[i].AddItem(Inventory.instance.items[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }

        }

    }
}
