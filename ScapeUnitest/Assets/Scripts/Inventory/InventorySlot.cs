﻿
using UnityEngine;
using UnityEngine.UI;
public class InventorySlot : MonoBehaviour
{
    Item item;
    public Image icon;
    public Button removeButton;
    public Text NumItems;
   // public int n;
    
    
    public void AddItem(Item newItem)
    {
        item = newItem;
        icon.sprite = item.icon;
        icon.enabled = true;
      //  item.cant = n;
        if (item.cant<item.maxcant)
        {
                NumItems.text = item.cant.ToString();
        }
        else if (item.cant==item.maxcant)
        {
            item.numItem = item.maxcant;
            NumItems.text = item.numItem.ToString();
        }
       
        removeButton.interactable = true;

       
    }

    public void ClearSlot(){
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
}

    public void OnRemoveButton()
    {
        Inventory.instance.Remove(item);
    }

    public void UseItem()
    {
        if (item!=null)
        {
            item.Use();
        }
    }
}
