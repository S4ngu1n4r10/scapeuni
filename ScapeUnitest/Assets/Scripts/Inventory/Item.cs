﻿
using UnityEngine;

[CreateAssetMenu(fileName ="new item",menuName ="inventory/item")]

public class Item : ScriptableObject
{
    // Start is called before the first frame update
    new public string name = "new Item";
    public Sprite icon = null;
    public bool isDefaultItem = false;
    public bool stackable;
    public int cant=0;
    public bool hP;
    public int maxcant=5;
    public int numItem;

    public virtual void Use()
    {
        if (hP)
        {
            Debug.Log("tecuro");
           
        }
        Debug.Log("using" + name);
    }
}
