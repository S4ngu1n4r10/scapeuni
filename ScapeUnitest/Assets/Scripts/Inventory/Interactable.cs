﻿using UnityEngine;
using UnityEngine.AI;

/*	
	This component is for all objects that the player can
	interact with such as enemies, items etc. It is meant
	to be used as a base class.
*/

//[RequireComponent(typeof(ColorOnHover))]
public class Interactable : MonoBehaviour
{

    public float radius = 3f;
 //   public Transform interactionTransform;

   // bool isFocus = false;   // Is this interactable currently being focused?
    Transform player;       // Reference to the player transform
    private void Awake()
    {
        player = FindObjectOfType<PlayerController>().transform;
    }
    //  bool hasInteracted = false; // Have we already interacted with the object?
    public virtual void Interact()
    {
      
        // This method is meant to be overwritten
        Debug.Log("Interacting with " + transform.name);
    }
    void Update()
    {
       // if (isFocus)    // If currently being focused
      //  {
            float distance = Vector3.Distance(player.position, transform.position);
            // If we haven't already interacted and the player is close enough
            if (distance <= radius)
            {
         
            // Interact with the object
            Debug.Log("choqueitem");
                Interact();
            }
        }
    

    // Called when the object starts being focused
  

    // This method is meant to be overwritten
 

  void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
 
    Gizmos.DrawWireSphere(transform.position, radius);
    }

}