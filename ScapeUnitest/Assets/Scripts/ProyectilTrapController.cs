﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilTrapController : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    private Transform Target;
    private PlayerController player;
    [SerializeField]
    private int MagicDamage = 50;
    // Start is called before the first frame update
    void Start()
    {
        Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
        speed = Random.Range(5, 10);
        float rand = Random.Range(-10, 10);
        rb.velocity = new Vector2(rand*speed, 0f);

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {


            player = collision.GetComponent<PlayerController>();
            player.TakeDamage(MagicDamage);
            Destroy(gameObject);
            /*
            enemy = collision.GetComponent<EnemyIA>();
            if ((enemy.currentHealth- MagicDamage) <= 0)
            {
                Debug.Log("memuero");
                enemy.barra.fillAmount = enemy.currentHealth / 100;
                enemy.enemyDead = true;
                Destroy(gameObject);

            }
           else if ((enemy.currentHealth -MagicDamage)> 0)
            {
                Debug.Log("mehacendaños");
                enemy.enemyHealth -= MagicDamage; print("Enemigo hit, salud:" + enemy.enemyHealth.ToString());
                enemy.barra.fillAmount = enemy.currentHealth / 100f;
                Destroy(gameObject);
            }
        */
        }
        if (collision.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }

        if (collision.CompareTag("Door"))
        {
            Destroy(gameObject);
        }
    }
}   

