﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapController : MonoBehaviour
{
    public Transform shootpoint;
    public GameObject proyectil;
    public float timer;
    public float cooldownTimer;
    // Start is called before the first frame update
    void Start()
    {
        cooldownTimer = 3;
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {cooldownReset();
        timer += Time.deltaTime;
     
        if (timer>=cooldownTimer)
        {
            Disparo(); 
        }
       
    }


    private void Disparo()
    {
        timer = 0;
        GameObject SkillInstance = Instantiate(proyectil, shootpoint.position, Quaternion.identity);
       
    }

    private void cooldownReset()
    {    if (cooldownTimer > 0)
        {
            cooldownTimer -= Time.deltaTime;
        }
       
        if (cooldownTimer < 0)
        {
            cooldownTimer = 0;
        }
    }
}
