﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyFloorController : MonoBehaviour
{
    public GameObject TileFloor;
    public LayerMask floor;
    public float maxX;
    public float minY;


    // Start is called before the first frame update
    void Start()
    {
        maxX = transform.position.x + 59;
        minY = transform.position.y  -59;
        Vector2 actualpos = transform.position;

        for (int i = 0; i < maxX; i++)
        {
            for (int y = 0; y > minY; y--)
            {
                Instantiate(TileFloor,transform.position, Quaternion.identity);
                Vector2 pos = new Vector2(transform.position.x + i, transform.position.y - y);
                transform.position = pos;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
/*
        if (transform.position.x <maxX)
        {
            FloorDetect();
        }
        if (transform.position.x==maxX&&transform.position.y >minY)
        {
            baja();
        }
*/
    }


    private void FloorDetect()
    {

 Collider2D floorDetect = Physics2D.OverlapCircle(transform.position, 1, floor);
        
               // Collider2D floorDetect = Physics2D.OverlapCircle(transform.position, 1, floor);
                if (floorDetect == null)
                {
                    Debug.Log("no suelo, pongo suelo");
                    Instantiate(TileFloor, transform.position, Quaternion.identity);
                    Vector2 newpos = new Vector2(transform.position.x+1 , transform.position.y);
                    transform.position = newpos;
                }
        else
        {
            Instantiate(TileFloor, transform.position, Quaternion.identity);
            Vector2 newpos = new Vector2(transform.position.x + 1, transform.position.y);
            transform.position = newpos;
        }
     }

    private void baja()
    {
        Vector2 newpos = new Vector2(transform.position.x, transform.position.y - 1);
        transform.position = newpos;
        Instantiate(TileFloor, transform.position, Quaternion.identity);
        
    }
}


        
    

    
