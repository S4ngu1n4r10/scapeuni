﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    public GameObject[] objects;
    private Transform parent;
    
   // public int maxObjects;
    
    // Start is called before the first frame update
    void Start()
    {
        parent = GetComponentInParent<Transform>();
       int rand = Random.Range(0, objects.Length);
       GameObject instance= Instantiate(objects[rand], transform.position, Quaternion.identity);
        instance.transform.SetParent(parent.transform.parent,true);
       Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
