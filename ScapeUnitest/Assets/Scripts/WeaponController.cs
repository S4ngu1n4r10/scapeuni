﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeaponController : MonoBehaviour
{
    private EnemyIA enemy;
    private Collider2D colWeapon;
    [SerializeField] private int WeaponDamage;
    // Start is called before the first frame update
    void Start()
    {
        WeaponDamage = 10;
        colWeapon = GetComponent<BoxCollider2D>();
        colWeapon.enabled = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        checkAttack();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Enemy"))
        {
            enemy = collision.GetComponent<EnemyIA>();
            if (enemy.enemyHealth -WeaponDamage <= 0)
            {
                Debug.Log("memuero");
                enemy.enemyDead = true;
                
            }
            if (enemy.enemyHealth > 0)
            {
                Debug.Log("mehacendaños");
                enemy.enemyHealth -= WeaponDamage; print("Enemigo hit, salud:" + enemy.enemyHealth.ToString());
                enemy.barra.fillAmount = enemy.enemyHealth / 100f;
            }

        }
    }
    private void checkAttack()
    {
        if (PlayerController.playerAttack)
        {
            colWeapon.enabled = true;
        }
        if (!PlayerController.playerAttack)
        {
            colWeapon.enabled = false;
        }


    }
}

