﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;
using DG.Tweening;



public class PlayerController : MonoBehaviour
{
    public static float timer;
    float directionX;
    Rigidbody2D rb;
    Animator anim;
    bool facingRight;
    bool AlreadyFacingRight;
    bool AlreadyFacingLeft;
    public static bool playerAttack = false;
    private float dir;
    private bool attacking;
    public static bool deadPlayer;
    private bool SpawnerInRange;
    public static float exp;
    public Text Gameover;
    public Text NoResource;
    public Text Exp;
    public Image BarraSalud;
    public Image BarraExp;
    public bool damaged;
    public Image BarraRecurso;
    public GameObject SkillMagic;
    public static int NumKeysG;
    public static int NumKeysGr;
    public static int NumKeysB;
    public static int NumKeysR;
    public Text KeysG;
    public Text KeysGr;
    public Text KeysB;
    public Text KeysR;
    public Text Coins;
    public Canvas FinJuego;
    private float TweenTimer;
    public static int NumCoins;
    public float limitExp;
    public int actualLevel;

    [Header("Stats")]
    public float TotalResource;
    public float currentResource;
    public float defense;
    public float totalHealth;
    public float currentHealth;
    public float cooldown;
    public float cooldownTimer;
    public float speed;
    private EnemyIA enemy;
    private SpawnEnemys sE;
    public int WeaponDamage;
    public float timeBetweenAttacks = 2;
    private bool enemyInRange;
    public float ExpAmountForLevel;
    public int multiplicadorLevel;
    public List<EnemyIA> Enemies = new List<EnemyIA>();
    private AudioManager audios;
 

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        deadPlayer = false;
        currentHealth = totalHealth;
        currentResource = TotalResource;
        NumKeysG = 0;
        NumKeysGr =0;
        NumKeysB = 0;
        NumKeysR = 0;
     
    
        cooldown = 2.0f;
        speed = 2;
        FinJuego.enabled = false;
        actualLevel = 1;
        ExpAmountForLevel = 100;
        multiplicadorLevel = 2;
        limitExp = 100;
        audios = FindObjectOfType<AudioManager>();
       // audios.Play("bso");
      

    }

    // Update is called once per frame
    void LateUpdate()
    {
        TweenTimer += Time.deltaTime;
        timer += Time.deltaTime;
        checkSalud();

        cooldownReset();

        timeattack();

        CheckKeys();
        Coins.text = NumCoins.ToString();
        
        checkExp();
        manageLevel();
    }

    private void cooldownReset() {
        if (cooldownTimer > 0)
        {
            cooldownTimer -= Time.deltaTime;
        }
        if (cooldownTimer < 0)
        {
            cooldownTimer = 0;
        }
        if (timeBetweenAttacks > 0)
        {
            timeBetweenAttacks -= Time.deltaTime;
        }
        if (timeBetweenAttacks < 0)
        {
            timeBetweenAttacks = 0;
        }


    }
    private void checkdead()
    {
        if (deadPlayer == true)
        {
            Time.timeScale = 0;
            FinJuego.enabled = true;
            Gameover.enabled = true;
        }
    }
    private void checkattack()
    {
        if (timer >= timeBetweenAttacks && enemyInRange && attacking==true)
        { timer = 0;

            foreach ( EnemyIA enemy in Enemies)
            {
                    enemy.TakeDamage(WeaponDamage);
                    audios.Play("Ataquenormal");
            }
           

          //  attacking = false;
           // playerAttack = false;
            timeBetweenAttacks = cooldown;

        }

        if (timer >= timeBetweenAttacks && SpawnerInRange && attacking==true)
        {
            sE.takedamage(WeaponDamage);
        }

        // If the player has zero or less health...

    }

    private void timeattack()
    {

        if (timeBetweenAttacks > 0)
        {
            timeBetweenAttacks -= Time.deltaTime;
        }
        if (timeBetweenAttacks < 0)
        {
            timeBetweenAttacks = 0;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Enemy"))
        {
            if (!Enemies.Contains(collision.GetComponent<EnemyIA>()))
            {
                  Enemies.Add(collision.GetComponent<EnemyIA>());
            }
        
           // Debug.Log("holaqueasesoyuneenemyquepasabaporaquitalcual");
            enemyInRange = true;
           // enemy = collision.GetComponent<EnemyIA>();
       
            {
               /* if (attacking)
                {
                    checkattack();
                }*/
               
            }

      
        }
        if (collision.CompareTag("Item"))        
           {
            Debug.Log("iteminteractable");
            ItemPickup iP= collision.GetComponent<ItemPickup>();
            if (Inventory.instance.objectCount < 15)
            {
                Inventory.instance.Add(iP.item);
                Destroy(collision.gameObject);

            }
            else if (Inventory.instance.objectCount ==Inventory.instance.spaceInv)
            {
                Debug.Log("no place for hideo");

            }
                
          
            
        }

        if (collision.CompareTag("EnemySpawner"))
        {
            SpawnerInRange = true;
           sE = collision.GetComponent<SpawnEnemys>();
           
           
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (!Enemies.Contains(collision.GetComponent<EnemyIA>()))
            {
                Enemies.Add(collision.GetComponent<EnemyIA>());
                enemyInRange = true;
            }
            else
            { return; }

        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            enemyInRange = false;
            playerAttack = false;
            Enemies.Clear();

        }

    }
    


    /*  public void Right()
      {
          if (deadPlayer == false)
          {
              if (transform.localScale == new Vector3(-5, 5, 1))
              {
              rb.MovePosition(transform.position+ new Vector3 (1,0,0));
          }
              else
              {
                  transform.localScale = new Vector3(-5, 5, 1);
              }

          }


      }

      public void Up()
      {
          if (deadPlayer == false)
          {
          rb.MovePosition(transform.position + new Vector3(0, 1, 0));
      }

      }

      public void Left()
      {
          if (deadPlayer == false)
          {
              if (transform.localScale == new Vector3(5, 5, 1))
              {
              rb.MovePosition(transform.position + new Vector3(-1, 0, 0));
          }
              else
              {
                  transform.localScale = new Vector3(5, 5, 1);
              }

          }




      }

      public void Down()
      {
          if (deadPlayer == false)
          {
          rb.MovePosition(transform.position + new Vector3(0, -1, 0));
      }
      }*/

    public void Attack()

    {
        anim.SetBool("Attack", true);
        playerAttack = true;
        if (timeBetweenAttacks == 0)
        {
            if (deadPlayer == false)
            {attacking = true;

               // checkattack();

            }

           
        } // anim.SetBool("Attack", playerAttack);
    }

    public void noAttack()
    {
        playerAttack = false;
       
        anim.SetBool("Attack",false);
    }

    public void SpawnMagic()
    {
        if (currentResource >= 20f && cooldownTimer == 0)
        {
            GameObject SkillInstance = Instantiate(SkillMagic, transform.position, Quaternion.identity);audios.Play("Habilidad"); anim.SetBool("Cast", false);
           
            BarraRecurso.transform.DOShakePosition(.4f, 2, 3, 1);

            currentResource -= 20f;
            BarraRecurso.fillAmount = currentResource / TotalResource;

            cooldownTimer = cooldown;
            anim.SetBool("Attack",false);
            return;
          

        }
        else if (currentResource <= 19f)
        {

            NoResource.enabled = true;
            print("noMANA");
        }

    }
    public void Skill()
    {audios.Play("LoadSkill");

        if (deadPlayer == false)
        {
            anim.SetBool("Cast", true);

        }
    }


    public void TakeDamage(float amount)
    {
        // Set the damaged flag so the screen will flash.
        damaged = true;
        // TweenMove(BarraSalud);
        //  TweenScale(BarraSalud);
        if (TweenTimer >= 1)
        {
            TweenTimer = 0;
            BarraSalud.transform.DOPunchScale(new Vector3(.1f, .1f), 1);audios.Play("DañoPlayer");
        }


        // Reduce the current health by the damage amount.
        float realdamage = defense * amount / 100;
        currentHealth = currentHealth - realdamage;

        // Set the health bar's value to the current health.
        BarraSalud.fillAmount = currentHealth / totalHealth;

        // Play the hurt sound effect.
        //  playerAudio.Play ();

        // If the player has lost all it's health and the death flag hasn't been set yet...
        if (currentHealth <= 0 && !deadPlayer)
        {
            currentHealth = 0;
            Debug.Log("player muere");
            // ... it should die.
            Death();
        }
    }

    private void Death()
    { anim.SetBool("Dead", true);
        deadPlayer = true;
        audios.stop("bso");
        audios.Play("dead");


        //  Destroy(gameObject,5);
    }

    private void checkSalud()
    {
        if (currentHealth > totalHealth)
        {
            currentHealth = totalHealth;
        }

        if (currentResource > TotalResource)
        {
            currentResource = TotalResource;
        }
    }

    public void StopAnim()
    {
        anim.SetBool("Cast", false);
        anim.SetBool("Attack", false);

    }

    private void checkExp()
    {
        Exp.text = "Level " + actualLevel.ToString();
        BarraExp.fillAmount = exp / limitExp;
        //BarraExp.transform.DOPunchScale(new Vector3(.1f, .1f), 1);
    }

    private void manageLevel()
    {
        if (exp>=limitExp)
        {
            audios.Play("LevelUp");
                actualLevel++;

          //  Exp.text = "Level " + actualLevel.ToString();
            
            BarraExp.fillAmount -= exp/limitExp ;exp = 0;
            limitExp += ExpAmountForLevel;
            WeaponDamage += 10;
            defense += 5;
            speed += .2f;
            TotalResource += 10;
            totalHealth += 10;
         
        }
    }



    private void CheckKeys()
    {
        KeysG.text = NumKeysG.ToString();
        KeysB.text = NumKeysB.ToString();
        KeysGr.text = NumKeysGr.ToString();
        KeysR.text = NumKeysR.ToString();
    }

    public void SavePlayer()
    {
        SaveManager.SavePlayer(this);

    }

    public void LoadPlayer()
    {
        CharacterData data = SaveManager.LoadPlayer();
        limitExp = data.limitexp;
        actualLevel = data.actuallevel;
        TotalResource = data.totalresource;
        totalHealth = data.totalhealth;
        speed = data.speed;
        ExpAmountForLevel = data.expamount;
        multiplicadorLevel = data.multilevel;
        defense = data.defense;
        WeaponDamage = data.weapondamage;
        NumCoins = data.coins;
        exp = data.exp;
        NumKeysB = data.numkeyB;
        NumKeysG = data.numkeyG;
        NumKeysGr = data.numkeyGr;
        NumKeysR = data.numkeyR;

    }

}


