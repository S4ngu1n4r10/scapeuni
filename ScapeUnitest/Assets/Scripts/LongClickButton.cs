﻿
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LongClickButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool pointerDown;
    private float pointerDownTimer;
    private Animator anim;
    public float requiredHoldTime;
    public UnityEvent OnLongClick;
    [SerializeField] Image fillImage;
    private void Start()
    {
        anim = GetComponentInParent<Animator>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDown = true;
     //   Debug.Log("Onpointerdown");
       
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Reset();
       // Debug.Log("OnPointerUp"); 

    }
    // Update is called once per frame
    void Update()
    {
        if (pointerDown)
        {
            
            pointerDownTimer += Time.deltaTime;
            if (pointerDownTimer > requiredHoldTime)
            {
            
                if (OnLongClick != null)
                {   
                    OnLongClick.Invoke();
                    Reset();
                }
            }
            fillImage.fillAmount = pointerDownTimer / requiredHoldTime;
        }
    }

    private void Reset()
    {
        pointerDown = false;
        pointerDownTimer = 0;
        fillImage.fillAmount = pointerDownTimer / requiredHoldTime;
    }


}
