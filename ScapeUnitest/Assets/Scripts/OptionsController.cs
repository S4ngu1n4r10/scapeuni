﻿
using UnityEngine;


public class OptionsController : MonoBehaviour
{
  public GameObject Inventory;
  
  public void enableInventory()
    {
        Time.timeScale = 0;
        Inventory.SetActive(true);

    }
    public void disableInventory()
    {
        Time.timeScale = 1;
        Inventory.SetActive(false);
    }

   


}
