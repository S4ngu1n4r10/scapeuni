﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFramerate : MonoBehaviour
{
    // Start is called before the first frame update
    private float timer;
    public int target = 60;
    void Start()
    {
        QualitySettings.vSyncCount = 0;
       // Application.targetFrameRate = 60;
            
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (target!=Application.targetFrameRate&&timer>=2)
        {
            Application.targetFrameRate = target;
        }
    }
}
