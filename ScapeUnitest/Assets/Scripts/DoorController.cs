﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    private bool open;
    public bool Red;
    public bool Gold;
    public bool Blue;
    public bool Green;
    // Start is called before the first frame update
    void Start()
    {
        open = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (Gold)
            {
                if (PlayerController.NumKeysG >= 1)
                {
                    open = true;
                    PlayerController.NumKeysG--;
                    Destroy(gameObject);
                }
                else if (PlayerController.NumKeysG <= 0)
                {
                    print("no hay llaves");
                }
            }

            if (Red)
            {
                if (PlayerController.NumKeysR >= 1)
                {
                    open = true;
                    PlayerController.NumKeysR--;
                    Destroy(gameObject);
                }
                else if (PlayerController.NumKeysR <= 0)
                {
                    print("no hay llaves");
                }
            }
            if (Blue)
            {
                if (PlayerController.NumKeysB >= 1)
                {
                    open = true;
                    PlayerController.NumKeysB--;
                    Destroy(gameObject);
                }
                else if (PlayerController.NumKeysB <= 0)
                {
                    print("no hay llaves");
                }
            }
            if (Green)
            {
                if (PlayerController.NumKeysGr >= 1)
                {
                    open = true;
                    PlayerController.NumKeysGr--;
                    Destroy(gameObject);
                }
                else if (PlayerController.NumKeysGr <= 0)
                {
                    print("no hay llaves");
                }
            }
        }
        }
    }
            
        
         
    

