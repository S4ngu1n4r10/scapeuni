﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickMoveController : MonoBehaviour
{
   
   [SerializeField] float maxSpeed;
    protected FixedJoystick joystick;
    public bool facingR;
    private SpriteRenderer player;
    Animator anim;
    private Rigidbody2D rb;
   


    // Start is called before the first frame update
    private void Start()
    {
        joystick = FindObjectOfType<FixedJoystick>();
        player = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
       rb = GetComponent<Rigidbody2D>();
     
   
    }
    // Update is called once per frame
    void FixedUpdate()
    {
      
        //  float moveX = Input.GetAxis("Horizontal");
        //float moveY = Input.GetAxis("Vertical");

        //Vector2 movement = new Vector2(moveX, moveY);
        //rb.AddForce(movement * maxSpeed); 
        if (PlayerController.deadPlayer==false)
        {
        rb.velocity = new Vector2(joystick.Horizontal * maxSpeed, joystick.Vertical * maxSpeed);
         }
       
            checkdir();
   }

    private void checkdir()
    {//flipx activado--> mirando a la izquierda
        //si voy a la derecha
        if (joystick.Horizontal>.1 )
        {
            anim.SetBool("IsWalking", true);
            facingR = false;
            player.flipX = false;

        }
        //si voy a la izquierda
        if (joystick.Horizontal<-.1 )

        {
            anim.SetBool("IsWalking", true);
            facingR = true;
            player.flipX = true;
        }

        if (joystick.Horizontal==0)
        {
            anim.SetBool("IsWalking", false);
            player.flipX = facingR;
        }

        if (joystick.Vertical>.1|| joystick.Vertical<-.1)
        {
            anim.SetBool("IsWalking", true);
        }
        //si me paro y estoy mirando a la izquierda
       
       

       
    }

    
}
