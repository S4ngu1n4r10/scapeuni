﻿using System;
using UnityEngine;

[Serializable]

public class CharacterData
{
   
    public float exp;
    public int numkeyG;
    public int numkeyR;
    public int numkeyGr;
    public int numkeyB;
    public float limitexp;
    public int actuallevel;
    public float totalresource;
    public float totalhealth;
    public float speed;
    public float expamount;
    public int multilevel;
    public float defense;
    public int weapondamage;
    public int coins;

    public CharacterData(PlayerController player)
    {
       limitexp= player.limitExp;
       actuallevel = player.actualLevel;
       totalresource = player.TotalResource;
       totalhealth = player.totalHealth;
       speed = player.speed;
       expamount = player.ExpAmountForLevel;
        multilevel = player.multiplicadorLevel;
        defense = player.defense;
        weapondamage = player.WeaponDamage;
        coins = PlayerController.NumCoins;
        exp = PlayerController.exp;
        numkeyB = PlayerController.NumKeysB;
        numkeyG = PlayerController.NumKeysG;
        numkeyGr = PlayerController.NumKeysGr;
        numkeyR = PlayerController.NumKeysR;



        //  exp = player.exp


    }
  
}