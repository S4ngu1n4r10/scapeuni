﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProyectilController : MonoBehaviour
{

    public float speed;
    public Rigidbody2D rb;
    private Transform Target;
    private EnemyIA enemy;
    [SerializeField]
    private int MagicDamage = 50;
    private GameObject[] enemys;
    private SpriteRenderer pRender;
    private SpriteRenderer bRender;
    // Start is called before the first frame update
    void Start()
    {
        Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        pRender= GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        bRender = GetComponent<SpriteRenderer>();

        if( pRender.flipX == true)
        {
            bRender.flipX = true ;
            rb.AddForce(new Vector2(-10, 0)*speed);
        }
        else
        {
            bRender.flipX = false;
            rb.AddForce(new Vector2(10, 0)*speed);
        }
       
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {


           enemy= collision.GetComponent<EnemyIA>();
            enemy.TakeDamage(MagicDamage);
            //Destroy(gameObject);
            /*
            enemy = collision.GetComponent<EnemyIA>();
            if ((enemy.currentHealth- MagicDamage) <= 0)
            {
                Debug.Log("memuero");
                enemy.barra.fillAmount = enemy.currentHealth / 100;
                enemy.enemyDead = true;
                Destroy(gameObject);

            }
           else if ((enemy.currentHealth -MagicDamage)> 0)
            {
                Debug.Log("mehacendaños");
                enemy.enemyHealth -= MagicDamage; print("Enemigo hit, salud:" + enemy.enemyHealth.ToString());
                enemy.barra.fillAmount = enemy.currentHealth / 100f;
                Destroy(gameObject);
            }
        */}
        else if (collision.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }
    }
}
