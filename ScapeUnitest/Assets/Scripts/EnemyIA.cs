﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;
using DG.Tweening;

public class EnemyIA : MonoBehaviour
{
    public GameObject Player;
    public GameObject Proyectil;
    private Transform Target;
    public float speed;
    private int range = 4;
    private float distanciaMin = 1f;
    private Animator anim;
    public bool enemyDead;
    private Rigidbody2D rb;
    public Image barra;
    private float moveTimer;
    [SerializeField] private GameObject drop;
    public float enemyHealth;
    public float currentHealth;
    [SerializeField] private float enemyDamage = 10f;
    private PlayerController player;
    private bool PlayerInRange;
    private bool damaged;
    float level;
    public Text DamagePoints;
    public float enemyExp;

    public float timeBetweenAttacks = 0.5f; // The time in seconds between each attack.

    float timer;

    // Start is called before the first frame update
    void Start()
    {
        enemyExp = 10;
        level = Random.value;
        player = FindObjectOfType<PlayerController>();
        Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        anim = GetComponent<Animator>();
        anim.SetBool("Attack", false);
        enemyDead = false;
        rb = GetComponent<Rigidbody2D>();

        vagabundea();
        PutStatsByLevel();
   


    }



    // Update is called once per frame
    void Update()
    {
        moveTimer += Time.deltaTime;

        //  float playerX = Target.transform.position.x;
        //float playerY = Target.transform.position.y;

        checkDead();
        if (moveTimer>=1)
        {
            Movimiento();
        }
     
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if (timer >= timeBetweenAttacks && PlayerInRange && enemyHealth > 0)
        {
            // ... attack.
            Attack();
        }

        // If the player has zero or less health...
        if (player.currentHealth <= 0)
        {
            // ... tell the animator the player is dead.
            // anim.SetTrigger("PlayerDead");
        }




    }

   private void vagabundea()
    {
        int randX = Random.Range(-5, 5);
        //Debug.Log(randX);
        int randY = Random.Range(-5, 5);
        //Debug.Log(randY);
        
            rb.AddForce( new Vector2(randX,randY)*speed);
            moveTimer = 0;
        
       
    }

    private void PutStatsByLevel()
    {
        if (level<.5f)
        {
            enemyDamage*= 2 ;
            enemyHealth *= 2;
            currentHealth = enemyHealth;
            enemyExp *= 2;
        }
        else if (level>.5f)
        {
            enemyDamage *= 3 ;
            enemyHealth *=3 ;
            currentHealth = enemyHealth;
            enemyExp *= 3;
        }
    }

    /*  private void Shoot()
      {
         // GameObject instance = Instantiate(Proyectil, transform.position, Quaternion.identity);
         // instance.transform.position = transform.Translate(Player.transform.position);
      }



      private void OnTriggerStay2D(Collider2D collision)
      {
          //  Debug.Log("ATACANDO"); 
          if (collision.CompareTag("Player"))
          {
                  Attack();
          }

      }
      */


    private void checkDead()
    {
        if (enemyDead==true)
        {
            PlayerController.exp += enemyExp;
            Instantiate(drop, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    private void Movimiento()
    {

        if (Vector2.Distance(transform.position, Target.position) > range)
        {
            //Debug.Log("estoy lejos");
            anim.SetBool("Attack", false);
            if (moveTimer >= 1)
            {
                vagabundea();

            }




        }
        else if (Vector2.Distance(transform.position, Target.position) > distanciaMin)
        {  
            transform.position = Vector2.MoveTowards(transform.position, Target.position, speed * Time.deltaTime);
          
        }


    }

  

    private void OnTriggerEnter2D(Collider2D collision)

    {
        GameObject debug = collision.gameObject;
        
        if (collision.gameObject==Player|| collision.CompareTag("Player"))
        {//Debug.Log("colision de enemy con ",debug);
            PlayerInRange = true;
            anim.SetBool("Attack", true);
            Attack();
            
            
         /*   PlayerInRange = true;
            anim.SetBool("Attack", true);
            Debug.Log("enemytocaplayer");
            // InvokeRepeating("Attack", 1, 3);
            if (player.totalHealth > 0f)
            {
          
                Debug.Log("HAGO DAÑO");
                player.totalHealth -= 10f;
                player.BarraSalud.fillAmount = player.totalHealth / 100f;

            }
            else if (player.totalHealth <= 0f)
            {
                print("estas muerto player");

            }*/
        }
        else
        {
            return;
        }


    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject==Player|| other.CompareTag("Player"))
        {
             PlayerInRange = true;
             anim.SetBool("Attack", true);
        }
        else
        {
            return;
        }
       
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject==Player|| collision.CompareTag("Player"))
        {
            PlayerInRange = false;
            //Debug.Log("dejo de atacar");
            anim.SetBool("Attack", false);
        }

    }

    void Attack()
    {
        // Reset the timer.
        timer = 0f;

        // If the player has health to lose...
        if (player.currentHealth > 0)
        {
            // ... damage the player.
            player.TakeDamage(enemyDamage); DamagePoints.enabled = true;
            DamagePoints.text = "Daño " + enemyDamage;
           
            Invoke("deactivatePoints",1f);

        }
    }

    private void deactivatePoints()
    {
        DamagePoints.enabled = false;
    }

    public void TakeDamage(float amount)
    {
        if (!enemyDead)
        {

        
        
        // Set the damaged flag so the screen will flash.
        damaged = true;



            if (barra != null)
            {
                barra.transform.DOPunchScale(new Vector2(.1f,.1f), .4f, 1, 1);
              

              //  TweenMove(barra);
             //   TweenScale(barra);
            }
        // Reduce the current health by the damage amount.
        currentHealth -= amount;
       
        
        // Set the health bar's value to the current health.
        barra.fillAmount = currentHealth /enemyHealth ;
       
        // Play the hurt sound effect.
        //  playerAudio.Play ();
        if ((currentHealth-amount)<=0)
            {
                currentHealth = 0;
                barra.fillAmount = currentHealth / enemyHealth;
                enemyDead = true;

        }

        // If the player has lost all it's health and the death flag hasn't been set yet...
      }
    }

 


}

    
    


    




