﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralOcClulling : MonoBehaviour
{
   // public GameObject[] rooms;
    // Start is called before the first frame update
    void Start()
    {
       //rooms = GameObject.FindGameObjectsWithTag("Room");
        /*foreach (GameObject room in rooms)
        {
            room.SetActive(false);
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            
            if (collision.GetComponent<EnemyIA>().enabled==false)
            {
                collision.GetComponent<EnemyIA>().enabled = true;
                collision.GetComponent<SpriteRenderer>().enabled = true;
                collision.GetComponent<Animator>().enabled = true;
                collision.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            }
        }

     /*   if (collision.tag =="Room")
        {
            
            collision.gameObject.SetActive(true);
            
        }*/
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
       /* if (collision.tag == "Room")
        {

            collision.gameObject.SetActive(true);

        }*/

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if (collision.GetComponent<EnemyIA>().enabled == true)
            {
                collision.GetComponent<EnemyIA>().enabled = false;
                collision.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<Animator>().enabled = false;
                collision.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

            }
        }

      /*  if (collision.tag == "Room")
        {

            collision.gameObject.SetActive(false);

        }*/
    }

}
